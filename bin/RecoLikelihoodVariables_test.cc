#include <iostream>
#include "TTH/CommonClassifier/interface/RecoLikelihoodVariables.h"
#include "TStopwatch.h"


const TLorentzVector p4(double pt, double eta, double phi, double mass) {
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}


int main(){

    auto jets_p4 = {
        p4(242.816604614, -0.107542805374, 1.25506973267, 24.5408706665),
        p4(191.423553467, -0.46368226409, 0.750520706177, 30.5682048798),
        p4(77.6708831787, -0.709680855274, -2.53739523888, 10.4904966354),
        p4(235.892044067, -0.997860729694, -2.10646605492, 27.9887943268),
        p4(52.0134391785, -0.617823541164, -1.23360788822, 6.45914268494),
        p4(35.6511192322, 0.566395223141, -2.51394343376, 8.94268417358),
    };

    auto leps_p4 = {
        p4(52.8751449585, -0.260020583868, -2.55171084404, 0.139569997787)
    };

    TLorentzVector lv_met;
    lv_met.SetPtEtaPhiM(92.1731872559,0., -1.08158898354, 0.);

    std::vector<double> csv = {0.88, 0.85, 0.9, 0.1, 0.3, 0.99};

    RecoLikelihoodVariables rlv;
    rlv.CalculateRecoLikelihoodVariables(leps_p4, jets_p4, csv, lv_met);

    std::map<TString,double> resultRecoLikelihoodVariables = rlv.GetRecoLikelihoodVariables();


    int max_varname_len = 0;
    for(auto var=resultRecoLikelihoodVariables.begin(); var!=resultRecoLikelihoodVariables.end(); var++){
        TString varname = var->first;
        int varname_len = varname.Sizeof();
        if(varname_len>max_varname_len) max_varname_len = varname_len;
    }

    std::cout << std::endl << "Number of reco likelihood variables: " << resultRecoLikelihoodVariables.size() << std::endl << std::endl << std::endl;
    std::cout << "Calculated reco likelihood variables:" << std::endl << std::endl;
    for(auto var=resultRecoLikelihoodVariables.begin(); var!=resultRecoLikelihoodVariables.end(); var++){
        TString varname = var->first;
        int varname_len = varname.Sizeof();

        std::cout << var->first;
        for(int j = 0; j<max_varname_len-varname_len; j++) std::cout << " ";
        std::cout << " = " << var->second << std::endl;
    }
    std::cout << std::endl;

    int ncalcs=100;
    std::cout<<"Calculating variables "<<ncalcs<<" times now"<<std::endl;
    TStopwatch* mytimer=new TStopwatch();
    mytimer->Start();
    for(int i=0; i<ncalcs;i++){
      rlv.CalculateRecoLikelihoodVariables(leps_p4, jets_p4, csv, lv_met);
    }
    mytimer->Stop();
    std::cout<<"Calculations took "<<std::endl;
    mytimer->Print();
    
    return 0;

}
