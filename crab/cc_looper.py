import ROOT, json, sys
import numpy as np
ROOT.gSystem.Load("libTTHCommonClassifier")

CvectorvectorLorentz = getattr(ROOT, "std::vector<vector<TLorentzVector>>")
CvectorLorentz = getattr(ROOT, "std::vector<TLorentzVector>")
Cvectordouble = getattr(ROOT, "std::vector<double>")
Cvectorvectordouble = getattr(ROOT, "std::vector<vector<double>>")
Cvectorvectorvectordouble = getattr(ROOT, "std::vector<vector<vector<double>>>")
CvectorJetType = getattr(ROOT, "std::vector<MEMClassifier::JetType>")
CvectorvectorJetType = getattr(ROOT, "std::vector<vector<MEMClassifier::JetType>>")
Cvectorbool = getattr(ROOT, "std::vector<bool>")
CvectorMEMResult = getattr(ROOT, "std::vector<MEMResult>")

"""
This code works as an interface to the MEMClassifier. It takes as input a ROOT Tree
containing the information about the objects in the event (kinematics, lepton charge,
jet systematics,...) and runs the MEMClassifier on those events. The required format of
the input tree can be found in the README. Different factorized JES sources are taken info
account and the MEM is calculated for all of them.
Note that the MEMClassifier can also work independantly of this looper, thus removing 
the need to produce an intermediate set of ntuples.
"""

def vec_from_list(vec_type, src):
    """
    Creates a std::vector<T> from a python list.
    vec_type (ROOT type): vector datatype, ex: std::vector<double>
    src (iterable): python list
    """
    v = vec_type()
    if vec_type == Cvectorvectordouble:
        for item in src:
            v2 = Cvectordouble()
            for item2 in item:
                v2.push_back(item2)
            v.push_back(v2)
    elif vec_type == Cvectorvectorvectordouble:
        for item in src:
            v2 = Cvectorvectordouble()
            for item2 in item:
                v3 = Cvectordouble()
                for item3 in item2:
                    v3.push_back(item3)
                v2.push_back(v3)
            v.push_back(v2)
    elif vec_type == CvectorvectorJetType:
        for item in src:
            v2 = CvectorJetType()
            for item2 in item:
                v2.push_back(item2)
            v.push_back(v2)
    else:
        for item in src:
            v.push_back(item)

    return v

jet_corrections = [        
            "AbsoluteStat",
            "AbsoluteScale",
            "AbsoluteFlavMap",
            "AbsoluteMPFBias",
            "Fragmentation",
            "SinglePionECAL",
            "SinglePionHCAL",
            "FlavorQCD",
            "TimePtEta",
            "RelativeJEREC1",
            "RelativeJEREC2",
            "RelativeJERHF",
            "RelativePtBB",
            "RelativePtEC1",
            "RelativePtEC2",
            "RelativePtHF",
            "RelativeBal",
            "RelativeFSR",
            "RelativeStatFSR",
            "RelativeStatEC",
            "RelativeStatHF",
            "PileUpDataMC",
            "PileUpPtRef",
            "PileUpPtBB",
            "PileUpPtEC1",
            "PileUpPtEC2",
            "PileUpPtHF",
            "JER"
        ]

#Jet selection criteria 
class Jets:
    jets = {
            # pt, |eta| thresholds for **leading two jets** (common between sl and dl channel)
            "pt":   30,
            "eta":  2.4,

            # pt, |eta| thresholds for **leading jets** specific to sl channel
            "pt_sl":  30,
            "eta_sl": 2.4,

            # pt, |eta| thresholds for **trailing jets** specific to dl channel
            "pt_dl":  20,
            "eta_dl": 2.4,

        }


def main(infile_name, firstEvent, lastEvent, outfile_name, conf):
    """
    Processes an input file with the CommonClassifier, saving the output in a file.
    infile_name (string): path to input file, can be root://, file://, ...
    firstEvent (int): first event to process (inclusive)
    lastEvent (int): last event to process (inclusive)
    outfile_name (string): output file name, must be writeable
    conf (dict): configuration dictionary
    """
    firstEvent = long(firstEvent)
    lastEvent  = long(lastEvent)

    #create the MEM classifier, specifying the verbosity and b-tagger type
    cls_mem = ROOT.MEMClassifier(0, conf["btag"])

    btagWP = conf["btagWP"]

    #one file
    if isinstance(infile_name, basestring):
        infile = ROOT.TFile.Open(infile_name)
        tree = infile.Get("tree")
    #many files
    else:
        tree = ROOT.TChain("tree")
        for fi in infile_name:
            tree.AddFile(fi)

    #create the output
    outfile_name = outfile_name
    outfile = ROOT.TFile(outfile_name, "RECREATE")

    #create the output TTree structure
    outtree = ROOT.TTree("tree", "CommonClassifier output tree")
    bufs = {}
    bufs["event"] = np.zeros(1, dtype=np.int64)
    bufs["run"] = np.zeros(1, dtype=np.int64)
    bufs["lumi"] = np.zeros(1, dtype=np.int64)
    bufs["systematic"] = np.zeros(1, dtype=np.int64)
    bufs["hypo"] = np.zeros(1, dtype=np.int64)
    bufs["mem_p"] = np.zeros(1, dtype=np.float64)
    bufs["mem_p_sig"] = np.zeros(1, dtype=np.float64)
    bufs["mem_p_bkg"] = np.zeros(1, dtype=np.float64)
    bufs["blr_4b"] = np.zeros(1, dtype=np.float64)
    bufs["blr_2b"] = np.zeros(1, dtype=np.float64)
    for unc in jet_corrections:
        for ud in ["Up","Down"]:
            bufs["mem_{}{}_p".format(unc,ud)] = np.zeros(1, dtype=np.float64)



    
    outtree.Branch("event", bufs["event"], "event/L")
    outtree.Branch("run", bufs["run"], "run/L")
    outtree.Branch("lumi", bufs["lumi"], "lumi/L")
    outtree.Branch("systematic", bufs["systematic"], "systematic/L")
    #outtree.Branch("hypo", bufs["hypo"], "hypo/L")

    outtree.Branch("mem_p", bufs["mem_p"], "mem_p/D")
    outtree.Branch("mem_p_sig", bufs["mem_p_sig"], "mem_p_sig/D")
    outtree.Branch("mem_p_bkg", bufs["mem_p_bkg"], "mem_p_bkg/D")
    
    for unc in jet_corrections:
        for ud in ["Up","Down"]:
            outtree.Branch("mem_{}{}_p".format(unc,ud), bufs["mem_{}{}_p".format(unc,ud)], "mem_{}{}_p/D".format(unc,ud))

    outtree.Branch("blr_4b", bufs["blr_4b"], "blr_4b/D")
    outtree.Branch("blr_2b", bufs["blr_2b"], "blr_2b/D")

    print "looping over event range [{0}, {1}]".format(firstEvent, lastEvent)
    if lastEvent<0:
        lastEvent = tree.GetEntries() - 1
    for iEv in range(firstEvent, lastEvent+1):

        evt_ret = tree.GetEntry(iEv)
        if evt_ret <= 0: continue

        print '\n'+'>>> Event #'+str(iEv)+'\n'

        bufs["event"][0] = tree.event
        bufs["run"][0] = tree.run
        bufs["lumi"][0] = tree.lumi
        bufs["systematic"][0] = tree.systematic

        if hasattr(tree, "hypothesis"):
            hypo = tree.hypothesis
        elif hasattr(tree, "hypo"):
            hypo = tree.hypo
        else:
            hypo = -1
        bufs["hypo"][0] = hypo
        ##############################################
        njets = tree.njets

        jets_unc = {}


        #process jets
        jets_p4 = CvectorvectorLorentz()
        jets_pt = list(tree.jet_pt)
        jets_eta = list(tree.jet_eta)
        jets_phi = list(tree.jet_phi)
        jets_mass = list(tree.jet_mass)
        jets_corr = list(tree.jet_corr)
        jets_corr_JER = list(tree.jet_corr_JER)

        for unc in jet_corrections:
            for ud in ["Up","Down"]:
                jets_unc["{}{}".format(unc,ud)] = list(getattr(tree,"jet_{a}{b}".format(a = unc, b = ud)))

        nBCSVM = 0


        jets_p4_nominal = CvectorLorentz()

        for iJet in range(njets):
            v = ROOT.TLorentzVector()
            v.SetPtEtaPhiM(jets_pt[iJet], jets_eta[iJet], jets_phi[iJet], jets_mass[iJet])
            jets_p4_nominal.push_back(v)

        jets_p4.push_back(jets_p4_nominal)

        for unc in jet_corrections:
            for ud in ["Up","Down"]:
                jets_p4_syst = CvectorLorentz()
                for iJet in range(njets):
                    v = ROOT.TLorentzVector()
                    corrfactor = getattr(tree,"jet_{a}{b}".format(a = unc, b = ud))[iJet]
                    pt = jets_pt[iJet]*corrfactor
                    mass = jets_mass[iJet]*corrfactor
                    v.SetPtEtaPhiM(pt, jets_eta[iJet], jets_phi[iJet], mass)
                    jets_p4_syst.push_back(v)
                jets_p4.push_back(jets_p4_syst)


        jv = []
        for iJet in range(njets):
            l  = []
            for unc in jet_corrections:
                for ud in ["Up","Down"]:
                    l.append(float(jets_unc["{}{}".format(unc,ud)][iJet]))
            jv.append(l)


        #Copy for each jet correction - not nice, but needed since for different corrections different jets can be deleted
        jvfull = []
        for i in range(len(jet_corrections)*2+1):
            jvfull.append(jv)

        jets_variations = vec_from_list(Cvectorvectorvectordouble, jvfull) 

        jettypes = []
        for iJet in range(njets):
            jettypes.append(0) 

        jcsvfull = []
        jdeepcsvfull = []
        jcmvafull = []
        jtypefull = []
        for i in range(len(jet_corrections)*2+1):
            jcsvfull.append(list(tree.jet_csv))
            jdeepcsvfull.append(list(tree.jet_deepcsv))
            jcmvafull.append(list(tree.jet_cmva))
            if hasattr(tree, 'jet_type'):
                jtypefull.append(list(tree.jet_type))
            else:
                jtypefull.append(jettypes)

        jets_csv = vec_from_list(Cvectorvectordouble, jcsvfull)
        jets_deepcsv = vec_from_list(Cvectorvectordouble, jdeepcsvfull)
        jets_cmva = vec_from_list(Cvectorvectordouble, jcmvafull)
        jets_type = vec_from_list(CvectorvectorJetType, jtypefull)



        ### JET selection
        for nj in range (0,len(jet_corrections)*2+1): #Loop over all systematics
            indexes = []
            good_jets = []
            #Apply jet selection criteria
            if tree.nleps==2:
                good_jets_dl = [] 
                counter = -1
                for jet in jets_p4[nj]:
                    counter += 1
                    if len(good_jets_dl) < 2:
                        ptcut = Jets.jets["pt_sl"]
                    else:
                        ptcut = Jets.jets["pt_dl"]
                    if jet.Pt() > ptcut:
                        etacut = Jets.jets["eta_dl"]
                        if abs(jet.Eta()) < etacut:
                            good_jets_dl += [jet]
                            indexes.append(counter)
                good_jets= good_jets_dl

            elif tree.nleps==1:
                ptcut = Jets.jets["pt_sl"]
                etacut = Jets.jets["eta_sl"]
                counter = -1
                for jet in jets_p4[nj]:
                    counter += 1
                    if jet.Pt() > ptcut and abs(jet.Eta()) < etacut:
                        good_jets += [jet]
                        indexes.append(counter)
            #Remove jets not fullfilling the criteria
            for i in range(jets_p4[nj].size()-1,-1,-1):
                if i not in indexes:
                    jets_p4[nj].erase(jets_p4[nj].begin()+i)
                    jets_variations[nj].erase(jets_variations[nj].begin()+i)
                    jets_csv[nj].erase(jets_csv[nj].begin()+i)
                    jets_deepcsv[nj].erase(jets_deepcsv[nj].begin()+i)
                    jets_cmva[nj].erase(jets_cmva[nj].begin()+i)
                    jets_type[nj].erase(jets_type[nj].begin()+i)
                    if nj == 0:
                        jets_pt.pop(i)
                        jets_eta.pop(i)
                        jets_phi.pop(i) 
                        jets_mass.pop(i)
                        jets_corr.pop(i)
                        jets_corr_JER.pop(i)

        njets = len(jets_p4[0])

        #choose which b-tagger to use
        jets_tagger = None
        if conf["btag"] == "btagCSV_":
            jets_tagger = jets_csv
        elif conf["btag"] == "btagDeepCSV_":
            jets_tagger = jets_deepcsv
        elif conf["btag"] == "btagBDT_":
            jets_tagger = jets_cmva


        for iJet in range(njets):  
            #FIXME FIXME dirty hack to count CSVM and exclude 2-tag events from MEM calculation
            if jets_tagger[0][iJet] > btagWP:
                nBCSVM += 1
               
        changes = []
        changes.append(False)
        count = 0
        #Verify for each factorized JES if the event category was changed
        for unc in jet_corrections:
            for ud in ["Up","Down"]:
                njetssys = jets_p4[count+1].size()
                nBCSVMsys = 0
                for iJet in range(njetssys):  
                    if jets_tagger[count+1][iJet] > btagWP:
                        nBCSVMsys += 1
                if njetssys != njets or nBCSVMsys != nBCSVM:
                    changes.append(True)
                else:
                    changes.append(False)

                count += 1

        changes_jet_category = vec_from_list(Cvectorbool, changes)


        if nBCSVM == 2:
            hypo = -2
            bufs["hypo"][0] = hypo
   
        #process leptons
        nleps = tree.nleps
        leps_p4 = CvectorLorentz()
        leps_pt = list(tree.lep_pt)
        leps_eta = list(tree.lep_eta)
        leps_phi = list(tree.lep_phi)
        leps_mass = list(tree.lep_mass)
        leps_charge = vec_from_list(Cvectordouble, list(tree.lep_charge))
        for ilep in range(nleps):
            v = ROOT.TLorentzVector()
            v.SetPtEtaPhiM(leps_pt[ilep], leps_eta[ilep], leps_phi[ilep], leps_mass[ilep])
            leps_p4.push_back(v)
    
        #process MET
        met = ROOT.TLorentzVector()
        met.SetPtEtaPhiM(
            tree.met_pt,
            0,
            tree.met_phi,
            0
        )
       
        bufs["mem_p"][0] = 0
        bufs["mem_p_sig"][0] = 0
        bufs["mem_p_bkg"][0] = 0
        bufs["blr_4b"][0] = 0
        bufs["blr_2b"][0] = 0
        for unc in jet_corrections:
            for ud in ["Up","Down"]:
                bufs["mem_{}{}_p".format(unc,ud)][0] = 0 
        #calculate the MEM
        
        if hypo >= -1 and njets > 0:
            ret = cls_mem.GetOutput(
                leps_p4,
                leps_charge,
                jets_p4,
                jets_tagger,
                jets_type,
                jets_variations,
                met,
                changes_jet_category
            )
            ##save the output
            bufs["mem_p"][0] = ret[0].p
            bufs["mem_p_sig"][0] = ret[0].p_sig
            bufs["mem_p_bkg"][0] = ret[0].p_bkg
            bufs["blr_4b"][0] = ret[0].blr_4b
            bufs["blr_2b"][0] = ret[0].blr_2b
            index = 0
            for unc in jet_corrections:
                for ud in ["Up","Down"]:
                    if changes[1+index] == True:
                        bufs["mem_{}{}_p".format(unc,ud)][0] = ret[1+index].p_variated[index]
                    else:
                        bufs["mem_{}{}_p".format(unc,ud)][0] = ret[0].p_variated[index]
                    index += 1



        outtree.Fill()
    
    outfile.Write()
    outfile.Close()

if __name__ == "__main__":

    #configurations go here
    confs = {
        "CSV": {
            "btag": "btagCSV_",
            "btagWP" : 0.8484
        },
        "DeepCSV": {
            "btag" : "btagDeepCSV_",
            "btagWP" : 0.4941
        },
        "CMVA": {
            "btag": "btagBDT_",
            "btagWP" : 0 #Not correct but we're not using it anyway
        },
    }

    import argparse
    parser = argparse.ArgumentParser(description='Calculates the CommonClassifier on a common input ntuple')
    parser.add_argument(
        '--infile', 
        action="store", 
        nargs='+', 
        help="Input file name (PFN)", 
        required=True
    )
    parser.add_argument(
        '--firstEvent', 
        action="store", 
        help="first event (by index) in tree to use", 
        type=long,
        default=0
    )
    parser.add_argument(
        '--lastEvent', 
        action="store", 
        help="last event (by index) in tree to use, inclusive (!)", 
        type=long,
        default=-1
    )
    parser.add_argument(
        '--maxEvents', 
        action="store", 
        help="total number of events to process", 
        type=long,
        required=False,
        default=None
    )
    parser.add_argument(
        '--outfile', 
        action="store", 
        help="output file name, must be writeable", 
        default="out.root"
    )
    parser.add_argument(
        '--conf', 
        type=str, 
        choices=sorted(confs.keys()), 
        default="DeepCSV"
    )
    args = parser.parse_args()
    conf = confs[args.conf]

    #use maxEvents if it was specified
    if not args.maxEvents is None:
        args.lastEvent = args.firstEvent + args.maxEvents - 1
    main(args.infile, args.firstEvent, args.lastEvent, args.outfile, conf)
